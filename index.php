<html>
<head>
	<title>UKK 2019</title>
	<style type="text/css">
	body {
		font-family: arial;
		font-size: 14px;
	}
	#canvas{
		background: url();
		background-size: 100%;
		width: 900px;
		margin: 0 auto;
		border: 1px solid silver;
	}
	#header{
		background: url(rental1.jpg);
		padding: 70px;
	}
	#menu{
		background-color: #0066ff;
	}
	#menu ul{
		list-style: none;
		margin: 0px;
		padding: 0px;
	}
	#menu ul li:hover{
		background-color: #0033cc;
	}
	#menu ul li.utama{
		display: inline-table;
	}
	#menu ul li a{
		display: block;
		text-decoration: none;
		line-height: 40px;
		padding: 0 10px;
		color: #fff;
	}
	.utama ul{
		display: none;
		position: absolute;
		z-index: 2px;
	}
	.utama:hover ul{
		display: block;
	}
	.utama ul li{
		display: block;
		background-color: #6cf;
		width: 140px;
	}
	#isi{
		min-height: 400px;
		padding: 20px;
	}
	#footer{
		background: url(rental1.jpg);
		padding: 70px;
	}
	</style>
</head>
<body>
	<div id="canvas">
		<div id="header">
			
		</div>
		<div id="menu">
			<ul>
				<li class="utama"><a href="?page=beranda">BERANDA</a></li>
				<li class="utama"><a href="">MOBIL RENTAL</a>
					<ul>
						<li><a href="?page=admin">LIAT DATA</a></li>
						<li><a href="?page=admin&action=tambah">TAMBAH DATA</a></li>
					</ul>
				</li>
				<li class="utama">
					<a href="?page=logout">LOGOUT</a>
				</li>
			</ul>
		</div>
		<div id="isi">
			<?php 
			$page = @$_GET['page'];
			$action = @$_GET['action'];
			if ($page == "admin") {
				if ($action == "") {
					include 'tampilan.php';
				}else if ($action == "tambah") {
					include 'tambah.php';
				}else if ($action == "edit") {
					include 'edit.php';
				}else if ($action == "hapus") {
					include 'hapus.php';
				}
			}else if ($page == "beranda") {
				if ($action == "") {
					include 'beranda.php';
				}
			}else if ($page == "logout") {
				if ($action == "") {
					include 'logout.php';
				}
			}
			?>
		</div>
		<div id="footer">
			
		</div>
	</div>
</body>
</html>